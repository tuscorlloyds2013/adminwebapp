import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { ArtistDTO } from '../modules/ArtistDTO';
import { AppConstants } from '../modules/AppConstants';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers : new HttpHeaders({
    'Content-Type' : 'application/json'
  })
}


@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  artistUrl : string = `${AppConstants.BACKEND_URL}${"admin"}`;
  pendingArtistsUrl : string = "getPendingArtists";
  confirmedArtistsUrl : string = "getConfirmedArtists";

  confirmArtistUrl : string = "confirmArtist?artistID=";
  rejectArtistUrl : string = "rejectArtist?artistID=";

  addArtistUrl : string = "addArtist";

  constructor(private http : HttpClient) { }

  getPendingArtists() : Observable<ArtistDTO[]> {
    return this.http.get<ArtistDTO[]>(`${this.artistUrl}/${this.pendingArtistsUrl}`);
  }

  getConfirmedArtists() : Observable<ArtistDTO[]> {
    return this.http.get<ArtistDTO[]>(`${this.artistUrl}/${this.confirmedArtistsUrl}`);
  }


  confrimArtist(artistID) {
    return this.http.get<boolean>(`${this.artistUrl}/${this.confirmArtistUrl}${artistID}`);
  }

  rejectArtist(artistID){
    return this.http.get<boolean>(`${this.artistUrl}/${this.rejectArtistUrl}${artistID}`);
  }

  addArtist(artistDTO : ArtistDTO){
    const url = `${this.artistUrl}/${this.addArtistUrl}`;
    console.log(url);
    return this.http.post(url,artistDTO,httpOptions);
  }

}
