export enum ImageType {
    PROFILE, ARTIST, EVENT, JPEG, PNG, LOCATION
}
