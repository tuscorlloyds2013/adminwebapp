import { Injectable } from '@angular/core';

import { HttpClient , HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppConstants } from '../modules/AppConstants';
import { AuthenticationResponse } from '../modules/AuthenticationResponse';
import { EventDTO } from '../modules/EventDTO';


const httpOptions = {
  headers : new HttpHeaders({
    'Content-Type' : 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class EventService {

  
  eventUrl : string = `${AppConstants.BACKEND_URL}${"admin"}`;
  getAllEventsUrl : string = "getAllEvents";
  getPendingEventsUrl : string = "getPendingEvents";
  getCreatedEventsUrl : string = "getCreatedEvents";
  getTargetBudgetAchievedUrl : string = "getTargetBudgetAchievedEvents";
  getPublishedEventsUrl : string = "getPublishedEvents";
  getBannedEventsUrl : string = "getBannedEvents";
  getCancelledEventsUrl : string = "getCancelledEvents";
  getHeldEventsUrl : string = "getHeldEvents";

  //////////////
  confirmedEventUrl : string = "bannedEvent?eventID="
  bannedEventUrl : string = "approvedEvent?eventID="

  private currentUserSubject: BehaviorSubject<AuthenticationResponse>;
  public currentUser: Observable<AuthenticationResponse>;

  constructor(private http: HttpClient) {
      this.currentUserSubject = new BehaviorSubject<AuthenticationResponse>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
  }

  getAllEvents() : Observable<EventDTO[]> {
    return this.http.get<EventDTO[]>(`${this.eventUrl}/${this.getAllEventsUrl}`);
  }

  getPendingEvents() : Observable<EventDTO[]> {
    return this.http.get<EventDTO[]>(`${this.eventUrl}/${this.getPendingEventsUrl}`);
  }

  getCreatedEvents() : Observable<EventDTO[]> {
    return this.http.get<EventDTO[]>(`${this.eventUrl}/${this.getCreatedEventsUrl}`);
  }

  getTargetBudgetAchieved() : Observable<EventDTO[]> {
    return this.http.get<EventDTO[]>(`${this.eventUrl}/${this.getTargetBudgetAchievedUrl}`);
  }

  getPublishedEvents() : Observable<EventDTO[]> {
    return this.http.get<EventDTO[]>(`${this.eventUrl}/${this.getPublishedEventsUrl}`);
  }


  getBannedEvents() : Observable<EventDTO[]> {
    return this.http.get<EventDTO[]>(`${this.eventUrl}/${this.getBannedEventsUrl}`);
  }

  getCancelledEvents() : Observable<EventDTO[]> {
    return this.http.get<EventDTO[]>(`${this.eventUrl}/${this.getCancelledEventsUrl}`);
  }

  getHeldEvents() : Observable<EventDTO[]> {
    return this.http.get<EventDTO[]>(`${this.eventUrl}/${this.getHeldEventsUrl}`);
  }


  ////////////////

  approveEvent(eventID : number) {
    return this.http.get<boolean>(`${this.eventUrl}/${this.confirmedEventUrl}${eventID}`);
  }

  bannedEvent(eventID : number){
    return this.http.get<boolean>(`${this.eventUrl}/${this.bannedEventUrl}${eventID}`);
  }
 

}
