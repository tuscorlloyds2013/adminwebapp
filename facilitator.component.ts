import { Component, OnInit, Input } from '@angular/core';
import { UserDTO } from 'src/app/modules/UserDTO';

@Component({
  selector: 'app-facilitator',
  templateUrl: './facilitator.component.html',
  styleUrls: ['./facilitator.component.css']
})
export class FacilitatorComponent implements OnInit {

  @Input() userDTO : UserDTO;

  name : string;
  email : string;
  contactNo : string;
  occupation : string;
  organization : string;

  constructor() { }

  ngOnInit() {
    this.name = this.userDTO.name;
    this.email = this.userDTO.email;
    this.contactNo = this.userDTO.contactNo;
    this.organization = this.userDTO.organization;
    this.occupation = this.userDTO.occupation;
  }

}
